export interface User {
  _id?: string
  firstName: string
  surname: string
  email: string
  profileImage: string
  password?: string
  token?: string
}

export interface Friend {
  _id?: string
  friendAccepted: boolean
  friendEmail: string
  userAccepted: boolean
}
