import Vue from 'vue'
import { Friend, User } from '@/core/interfaces/user.interface.ts'
import { Response } from '@/core/interfaces/response.interface'
import { FriendService } from '@/core/services/friends.service'
import { snackbarState } from '@/core/stores/snackbar.store'
import { FriendsListItem } from '@/components/FriendsList.vue'

const friendService = new FriendService()

export interface FriendsStore {
  friends: Friend[]
  users: { [email: string]: User }
  install: () => void
  clear: () => void
  populate: () => Promise<{
    friends: Friend[]
    users: { [email: string]: User }
  }>
  getFriends: () => Promise<{
    friendList: FriendsListItem[]
    users: { [email: string]: User }
  }>
}

export const friendsStore: FriendsStore = {
  friends: [],
  users: {},
  install: () => {
    Object.defineProperty(Vue.prototype, '$friends', {
      get() {
        return friendsStore
      }
    })
  },
  clear(): void {
    friendsStore.friends = []
    friendsStore.users = {}
  },
  async populate(): Promise<{
    friends: Friend[]
    users: { [email: string]: User }
  }> {
    if (friendsStore.friends.length === 0) {
      const resp: Response<
        Friend[],
        { friends?: { [email: string]: User } }
      > = await friendService.friends()

      if (!resp.error) {
        friendsStore.friends = resp.data as Friend[]
      } else {
        snackbarState.push(resp.msg as string, 'error')
      }

      if (resp.meta && resp.meta.friends) {
        friendsStore.users = resp.meta.friends
      }
    }

    return {
      friends: friendsStore.friends,
      users: friendsStore.users
    }
  },
  async getFriends(): Promise<{
    friendList: FriendsListItem[]
    users: { [email: string]: User }
  }> {
    await friendsStore.populate()

    const items: FriendsListItem[] = []
    friendsStore.friends.forEach((f: Friend) => {
      if (f.friendAccepted && f.userAccepted) {
        items.push({
          id: f._id as string,
          email: f.friendEmail as string,
          avatar: friendsStore.users[f.friendEmail].profileImage,
          title: `${friendsStore.users[f.friendEmail].firstName} ${
            friendsStore.users[f.friendEmail].surname
          }`,
          subtitle: `<span class="text--primary">Date</span>`,
          options: true
        })
      }
    })

    return {
      friendList: items,
      users: friendsStore.users
    }
  }
}
