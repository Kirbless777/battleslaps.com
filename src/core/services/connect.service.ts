import axios from 'axios'
import { User } from '@/core/interfaces/user.interface'
import { HttpResponse, Response } from '@/core/interfaces/response.interface'
import { snackbarState } from '@/core/stores/snackbar.store'
import {Game} from "@/core/interfaces/game.interface";

export interface HttpError {
  error: boolean
  msg: string
}

export class ConnectService {
  readonly headers: { [header: string]: string } = {
    'Content-Type': 'application/json'
  }

  private url = `https://y1ibxcu0nk.execute-api.eu-west-2.amazonaws.com/dev/connect-four`
  // private url = `http://localhost:3001/connect-four`;

  public async list(): Promise<
    Response<Game[], { friends?: { [email: string]: User } }>
  > {
    try {
      const resp: HttpResponse<
        Game[],
        { friends?: { [email: string]: User } }
      > = await axios.get(`${this.url}/list`, {
        headers: {
          ...this.headers,
          Authorization: sessionStorage.getItem('token')
        }
      })
      if (resp && resp.data && resp.status == 200 && resp.data.data) {
        return resp.data
      } else {
        return ConnectService.handleError({})
      }
    } catch (e) {
      return ConnectService.handleError(e)
    }
  }

  public async turn(cell: object): Promise<Response<Game, null>> {
    try {
      const resp = await axios.post(`${this.url}/turn`, cell, {
        headers: {
          ...this.headers,
          Authorization: sessionStorage.getItem('token')
        }
      })
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return ConnectService.handleError(e)
    }
  }

  public async win(id: string, email: string): Promise<Response<Game, null>> {
    try {
      const resp = await axios.post(`${this.url}/winner/${id}`, {email}, {
        headers: {
          ...this.headers,
          Authorization: sessionStorage.getItem('token')
        }
      })
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return ConnectService.handleError(e)
    }
  }

  public async create(email: string): Promise<Response<Game, null>> {
    try {
      const resp = await axios.post(
        `${this.url}`,
        {
          email
        },
        {
          headers: {
            ...this.headers,
            Authorization: sessionStorage.getItem('token')
          }
        }
      )
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return ConnectService.handleError(e)
    }
  }

  public async get(id: string): Promise<Response<Game, null>> {
    try {
      const resp = await axios.get(`${this.url}/game/${id}`, {
        headers: {
          ...this.headers,
          Authorization: sessionStorage.getItem('token')
        }
      })
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return ConnectService.handleError(e)
    }
  }

  public async getLastTurn(id: string): Promise<Response<Game, null>> {
    try {
      const resp = await axios.get(`${this.url}/last-turn/${id}`, {
        headers: {
          ...this.headers,
          Authorization: sessionStorage.getItem('token')
        }
      })
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return ConnectService.handleError(e)
    }
  }

  private static handleError<T, M>(
    e: {
      response?: { data?: { message?: string } }
    },
    hideSnack?: boolean
  ): Response<T, M> {
    if (e.response) {
      if (e.response.data && e.response.data.message) {
        if (!hideSnack) snackbarState.push(e.response.data.message, 'danger')
        return { error: true, msg: e.response.data.message, data: {} as T }
      }
    }
    if (!hideSnack) snackbarState.push('Oops, something when', 'danger')
    return { error: true, msg: 'Something went wrong', data: {} as T }
  }
}
