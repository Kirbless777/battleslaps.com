import Vue from 'vue'
import { User } from '@/core/interfaces/user.interface.ts'

interface Auth {
  isAuth: boolean
  user?: User
}

export interface AuthState {
  auth: Auth
  install: () => void
  update: (user: User) => void
}

export const authState: AuthState = {
  auth: {
    isAuth: false
  },
  install: () => {
    Object.defineProperty(Vue.prototype, '$auth', {
      get() {
        return authState
      }
    })
  },
  update(user: User) {
    authState.auth.isAuth = true
    authState.auth.user = user
  }
}
