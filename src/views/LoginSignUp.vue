<template>
  <v-container class="login">
    <v-row no-gutters>
      <v-col cols="12" md="8">
        <v-sheet elevation="1" max-width="444" class="pb-10">
          <v-img src="@/assets/images/batman-slap.jpg" height="250px"></v-img>
          <div class="ml-4 mr-4">
            <v-card
              class="login-card mx-auto"
              max-width="444"
              color=""
              elevation="10"
            >
              <v-card-title>
                {{ isSignUp ? 'Login' : 'Create Account' }}
              </v-card-title>

              <v-card-text v-if="isSignUp">
                <FormBuilder
                  :allow-validation="isSubmitted"
                  :fields="signInForm"
                  :data="signInData"
                  @check-validated="validated = $event"
                />
              </v-card-text>
              <v-card-text v-else>
                <FormBuilder
                  v-if="!signUpMessage"
                  :fields="signUpForm"
                  :data="signUpData"
                  @check-validated="validated = $event"
                />
                <p v-else>
                  {{ signUpMessage }}
                </p>
              </v-card-text>

              <v-card-actions>
                <div class="login-button">
                  <v-btn
                    class="pl-2"
                    block
                    rounded
                    :loading="isLoading"
                    @click="isSignUp ? submit() : signUp()"
                  >
                    {{ isSignUp ? 'Login' : 'Create Account' }}
                  </v-btn>
                </div>

                <v-spacer></v-spacer>
              </v-card-actions>
            </v-card>
          </div>

          <v-btn
            class="mt-2"
            color="warning"
            small
            block
            text
            @click="isSignUp = !isSignUp"
          >
            {{ isSignUp ? 'Create Account' : 'Login' }}
          </v-btn>
        </v-sheet>
      </v-col>
      <v-col cols="6" md="4"></v-col>
    </v-row>
  </v-container>
</template>

<script lang="ts">
import Vue from 'vue'
import FormBuilder from '@/components/FormBuilder.vue'
import { AuthService } from '@/core/services/auth.service'
import { User } from '@/core/interfaces/user.interface' // @ is an alias to /src
import { Response } from '@/core/interfaces/response.interface'

const authService = new AuthService()

export default Vue.extend({
  name: 'LoginSignUp',
  data() {
    return {
      isLoading: false,
      isSignUp: true,
      validated: null,
      isSubmitted: false,
      signInData: {
        email: '',
        password: ''
      },
      signUpData: {
        firstName: '',
        surname: '',
        email: '',
        profileImage: '',
        password: ''
      } as User,
      signInForm: {
        0: {
          type: 'text',
          field: 'email',
          required: true,
          rules: [
            (v: string) => !!v || 'E-mail is required',
            (v: string) => /.+@.+\..+/.test(v) || 'E-mail must be valid'
          ],
          label: 'Email'
        },
        1: {
          type: 'password',
          field: 'password',
          required: true,
          rules: [(v: string) => !!v || 'Password is required'],
          label: 'Password'
        }
      },
      signUpForm: {
        0: {
          type: 'text',
          field: 'firstName',
          required: true,
          rules: [(v: string) => !!v || 'Name is required'],
          label: 'First Name'
        },
        1: {
          type: 'text',
          field: 'surname',
          required: true,
          rules: [(v: string) => !!v || 'Surname is required'],
          label: 'Surname'
        },
        2: {
          type: 'text',
          field: 'email',
          required: true,
          rules: [
            (v: string) => !!v || 'E-mail is required',
            (v: string) => /.+@.+\..+/.test(v) || 'E-mail must be valid'
          ],
          label: 'Email'
        },
        3: {
          type: 'password',
          field: 'password',
          required: true,
          rules: [(v: string) => !!v || 'Password is required'],
          label: 'Password'
        }
      },
      signUpMessage: ''
    }
  },
  components: {
    FormBuilder
  },
  methods: {
    async submit() {
      this.isSubmitted = true
      if (this.validated || this.validated === null) {
        this.isLoading = true
        if (this.isSignUp) {
          const resp: Response<User, null> = await authService.login(
            this.signInData.email,
            this.signInData.password
          )
          if (!resp.error) {
            await this.$router.push({ path: '/' })
          }
          this.isLoading = false
        }
      }
    },
    signUp: async function() {
      // TODO - Auth...
      this.isLoading = true
      const resp: Response<User, null> = await authService.create(
        this.signUpData as User
      )
      this.isLoading = false
      if (!resp.error) {
        this.signUpMessage =
          'Users created! Check your email to verify your account.'
      }
    }
  }
})
</script>
<style lang="scss">
.login {
  min-height: 100vh;
}

.login-card {
  top: -24px;
}

.login-button {
  bottom: -20px;
  left: 24px;
  position: absolute;
  width: -webkit-calc(100% - 48px) !important;
  width: expression(100% - 48px) !important;
  width: -moz-calc(100% - 48px) !important;
  width: -o-calc(100% - 48px) !important;
  width: calc(100% - 48px) !important;
}
</style>
