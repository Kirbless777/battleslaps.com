export interface Response<T, M = object> {
  data: T
  meta?: M
  error?: boolean
  msg?: string
}

export interface HttpResponse<T, M = object> {
  data?: Response<T, M>
  status?: number
}
