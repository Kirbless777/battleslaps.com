import Vue from 'vue'
import VueRouter, { NavigationGuardNext, Route, RouteConfig } from 'vue-router'
import Home from '@/views/auth/Home.vue'
import LoginSignUp from '@/views/LoginSignUp.vue'
import Shell from '@/views/Shell.vue'
import { AuthService } from '@/core/services/auth.service'
import { snackbarState } from '@/core/stores/snackbar.store'
import NotFound from '@/views/NotFound.vue'
import { authState } from '@/core/stores/auth.store'
import { connectFourRoutes } from '@/views/auth/connect-four/connect-four.route'
import Verify from '@/views/Verify.vue'

Vue.use(VueRouter)

const auth = new AuthService()

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'LoginSignUp',
    component: LoginSignUp
  },
  {
    path: '/verify/:token',
    name: 'Verify',
    component: Verify
  },
  {
    path: '/',
    component: Shell,
    beforeEnter: async (to: Route, from: Route, next: NavigationGuardNext) => {
      if (!authState.auth.isAuth) {
        const user = await auth.me()
        if (user && !user.error) {
          next()
        } else {
          if (to.name != 'Home') {
            snackbarState.push('Oops, you need to sign in first!', 'warning')
          }
          next('/login')
        }
      } else {
        next()
      }
    },
    children: [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: '/coming',
        name: 'Coming Soon',
        component: () =>
          import(/* webpackChunkName: "about" */ '@/views/auth/ComingSoon.vue')
      },
      {
        path: '/profile',
        name: 'Profile',
        component: () =>
            import(/* webpackChunkName: "about" */ '@/views/auth/profile/Profile.vue')
      },
      {
        path: '/friends',
        name: 'Friends',
        component: () =>
          import(
            /* webpackChunkName: "about" */ '@/views/auth/friends/Friends.vue'
          )
      },
      ...connectFourRoutes
    ]
  },
  {
    path: '*',
    name: 'NotFound',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
