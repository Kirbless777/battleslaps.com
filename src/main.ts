import Vue from 'vue'
import App from './App.vue'
import router from './core/router'
import vuetify from './core/plugins/vuetify'
import { authState } from '@/core/stores/auth.store'
import { snackbarState } from '@/core/stores/snackbar.store'
import { friendsStore } from '@/core/stores/friends.store'

Vue.config.productionTip = false

Vue.use(authState)
Vue.use(snackbarState)
Vue.use(friendsStore)

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
