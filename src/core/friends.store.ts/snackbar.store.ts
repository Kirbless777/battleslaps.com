import Vue from 'vue'

export interface Snackbar {
  msg: string
  type: string
}

export interface SnackbarState {
  messages: Snackbar[]
  install: () => void
  push: (msg: string, type: string) => void
}

export const snackbarState: SnackbarState = {
  messages: [],
  install: () => {
    Object.defineProperty(Vue.prototype, '$snackbar', {
      get() {
        return snackbarState
      }
    })
  },
  push(msg: string, type: string) {
    snackbarState.messages.push({ msg, type })
    setTimeout(() => snackbarState.messages.pop(), 5000)
  }
}
