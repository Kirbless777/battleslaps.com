import axios from 'axios'
import { authState } from '@/core/stores/auth.store'
import { friendsStore } from '@/core/stores/friends.store'
import { User } from '@/core/interfaces/user.interface'
import { Response } from '@/core/interfaces/response.interface'
import { snackbarState } from '@/core/stores/snackbar.store'

export interface HttpError {
  error: boolean
  msg: string
}

export class AuthService {
  readonly headers: { [header: string]: string } = {
    'Content-Type': 'application/json'
  }

  private url = `https://y1ibxcu0nk.execute-api.eu-west-2.amazonaws.com/dev`
  // private url = `http://localhost:3001`

  public async me(): Promise<Response<User, null>> {
    try {
      const resp = await axios.get(`${this.url}/me`, {
        headers: {
          ...this.headers,
          Authorization: sessionStorage.getItem('token')
        }
      })
      if (resp && resp.data && resp.data.data.email) {
        this.handleAuthenticate(resp)
        return resp.data
      } else {
        return this.handleError({}, true)
      }
    } catch (e) {
      return this.handleError(e, true)
    }
  }

  public async verify(token: string): Promise<Response<User, null>> {
    try {
      const resp = await axios.get(`${this.url}/verify`, {
        headers: {
          ...this.headers,
          Authorization: token
        }
      })
      if (resp && resp.data) {
        this.saveToken(resp.data.data.token as string)
        this.handleAuthenticate(resp)
        return resp.data
      } else {
        return this.handleError({})
      }
    } catch (e) {
      return this.handleError(e)
    }
  }

  public async login(
    email: string,
    password: string
  ): Promise<Response<User, null>> {
    try {
      const resp = await axios.post(
        `${this.url}/signin`,
        {
          email,
          password
        },
        {
          headers: this.headers
        }
      )
      if (resp && resp.status === 200) {
        this.saveToken(resp.data.data.token as string)
        delete resp.data.data.token
        this.handleAuthenticate(resp)
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return this.handleError(e)
    }
  }

  public async getUsers(emails: string[]): Promise<Response<User[], null>> {
    try {
      const resp = await axios.post(
        `${this.url}/users`,
        {
          emails
        },
        {
          headers: {
            ...this.headers,
            Authorization: sessionStorage.getItem('token')
          }
        }
      )
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return this.handleError(e)
    }
  }

  public async create(user: User): Promise<Response<User, null>> {
    try {
      const resp = await axios.post(`${this.url}/user/create`, user, {
        headers: this.headers
      })
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return this.handleError(e)
    }
  }

  public logout(): void {
    sessionStorage.clear()
    friendsStore.clear()
    authState.logout()
  }

  private handleAuthenticate(resp: { data: { data: User } }): void {
    authState.update(resp.data.data)
  }

  private saveToken(token: string): void {
    sessionStorage.setItem('token', token)
  }

  private handleError<T, M>(
    e: {
      response?: { data?: { message?: string } }
    },
    hideSnack?: boolean
  ): Response<T, M> {
    if (e.response) {
      if (e.response.data && e.response.data.message) {
        if (!hideSnack) snackbarState.push(e.response.data.message, 'danger')
        return { error: true, msg: e.response.data.message, data: {} as T }
      }
    }
    if (!hideSnack) snackbarState.push('Oops, something when', 'danger')
    return { error: true, msg: 'Something went wrong', data: {} as T }
  }
}
