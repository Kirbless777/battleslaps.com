// import { reactive, computed, UnwrapRef, ComputedRef } from 'vue'
// import { User } from '@/core/interfaces/user.interface'
//
// interface Game {
//   currentGame: any
// }
//
// const state: UnwrapRef<Game> = reactive({
//   currentGame: undefined
// })
//
// // game
// const watchGame = computed((): any => state.currentGame)
// const setGame = (game: any): void => {
//   state.currentGame = game
// }
//
// export interface GameState {
//   watchGame: ComputedRef<boolean>
//   setGame: (a: boolean) => void
// }
//
// const gameState: GameState = {
//   watchGame,
//   setGame
// }
//
// export default gameState
