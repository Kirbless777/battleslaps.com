export const connectFourRoutes = [
  {
    path: '/connect-4',
    name: 'Connect 4',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '@/views/auth/connect-four/ConnectFour.vue'
      ),
    children: [
      {
        path: '',
        name: 'Connect 4 Landing',
        component: () =>
          import(
            /* webpackChunkName: "about" */ '@/views/auth/connect-four/children/ConnectFourLanding.vue'
          )
      },
      {
        path: ':id',
        name: 'Connect 4',
        component: () =>
          import(
            /* webpackChunkName: "about" */ '@/views/auth/connect-four/children/ConnectFourGame.vue'
          )
      }
    ]
  }
]
