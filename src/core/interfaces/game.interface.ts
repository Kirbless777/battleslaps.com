export interface Game {
    _id: string
    playerOne: string
    playerTwo: string
    playerOneColor: string
    playerTwoColor: string
    game: any
    winner?: string
    lastMove: {
        _id: string
        key: string
        value: string
    }
}
