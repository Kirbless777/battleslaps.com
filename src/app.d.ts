import Vue from 'vue'
import { AuthState } from '@/core/stores/auth.store'
import { FriendsStore } from '@/core/stores/friends.store'

declare module 'vue/types/vue' {
  interface Vue {
    $auth: AuthState
    $snackbar: SnackbarState
    $friends: FriendsStore
  }
}
