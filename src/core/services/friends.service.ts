import axios from 'axios'
import { Friend, User } from '@/core/interfaces/user.interface'
import { HttpResponse, Response } from '@/core/interfaces/response.interface'

export interface HttpError {
  error: boolean
  msg: string
}

export class FriendService {
  readonly headers: { [header: string]: string } = {
    'Content-Type': 'application/json'
  }

  private url = `https://y1ibxcu0nk.execute-api.eu-west-2.amazonaws.com/dev`

  // private url = `http://localhost:3001`;

  public async friends(): Promise<
    Response<Friend[], { friends?: { [email: string]: User } }>
  > {
    try {
      const resp: HttpResponse<
        object,
        { friends?: { [email: string]: User } }
      > = await axios.get(`${this.url}/friends`, {
        headers: {
          ...this.headers,
          Authorization: sessionStorage.getItem('token')
        }
      })
      if (resp && resp.data && resp.status == 200 && resp.data.data) {
        return resp.data as Response<
          Friend[],
          { friends?: { [email: string]: User } }
        >
      } else {
        return this.handleError<
          Friend[],
          { friends?: { [email: string]: User } }
        >({})
      }
    } catch (e) {
      return this.handleError<
        Friend[],
        { friends?: { [email: string]: User } }
      >(e)
    }
  }

  public async invite(email: string): Promise<Response<Friend, null>> {
    try {
      const resp = await axios.post(
        `${this.url}/invite`,
        {
          email
        },
        {
          headers: {
            ...this.headers,
            Authorization: sessionStorage.getItem('token')
          }
        }
      )
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return this.handleError<Friend, null>(e)
    }
  }

  public async accept(id: string): Promise<Response<Friend, User>> {
    try {
      const resp = await axios.post(
        `${this.url}/accept-invite`,
        {
          id
        },
        {
          headers: {
            ...this.headers,
            Authorization: sessionStorage.getItem('token')
          }
        }
      )
      if (resp && resp.status === 200) {
        return resp.data
      }
      throw 'error'
    } catch (e) {
      return this.handleError(e)
    }
  }

  private handleError<T, M>(e: {
    response?: { data?: { message?: string } }
  }): Response<T, M> {
    if (e.response) {
      if (e.response.data && e.response.data.message) {
        return { error: true, msg: e.response.data.message, data: {} as T }
      }
    }
    return { error: true, msg: 'Something went wrong', data: {} as T }
  }
}
